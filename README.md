# Magazine-API

This is a RESTful JSON API with Rails 5 

The app consists of magazines and articles.
They can be fetch, created, updated and destroyed through the API. 

Feel free to fork your own version and adapt it to your needs.