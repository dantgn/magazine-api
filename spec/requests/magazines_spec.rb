require 'rails_helper'

RSpec.describe 'Magazines API', type: :request do
  # initialize test data
  let!(:magazines) { create_list(:magazine, 10) }
  let(:magazine_id) { magazines.first.id }

  describe 'GET /magazines' do
    # make HTTP get request before each example
    before { get '/magazines' }

    it 'returns magazines' do
      expect(response_to_json).not_to be_empty
      expect(response_to_json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /magazines/:id' do
    before { get "/magazines/#{magazine_id}" }

    context 'when the record exists' do
      it 'returns the magazine' do
        expect(response_to_json).not_to be_empty
        expect(response_to_json['id']).to eq(magazine_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:magazine_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Magazine/)
      end
    end
  end

  describe 'POST /magazines' do
    # valid payload
    let(:valid_attributes) { { title: 'Travel and enjoy' } }

    context 'when the request is valid' do
      before { post '/magazines', params: valid_attributes }

      it 'creates a magazine' do
        expect(response_to_json['title']).to eq('Travel and enjoy')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/magazines', params: { title: "" } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Title can't be blank/)
      end
    end
  end

  describe 'PUT /magazines/:id' do
    let(:valid_attributes) { { title: 'Football friends' } }

    context 'when the record exists' do
      before { put "/magazines/#{magazine_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /magazines/:id' do
    before { delete "/magazines/#{magazine_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end