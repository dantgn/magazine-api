require 'rails_helper'

RSpec.describe 'Articles API' do
  # Initialize the test data
  let!(:magazine) { create(:magazine) }
  let!(:articles) { create_list(:article, 20, magazine_id: magazine.id) }
  let(:magazine_id) { magazine.id }
  let(:id) { articles.first.id }

  describe 'GET /magazines/:magazine_id/articles' do
    before { get "/magazines/#{magazine_id}/articles" }

    context 'when magazine exist' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all magazine articles' do
        expect(response_to_json.size).to eq(20)
      end
    end

    context 'when magazine does not exist' do
      let(:magazine_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Magazine/)
      end
    end
  end

  describe 'POST /magazines/:magazine_id/articles' do
    let(:valid_attributes) { { title: 'Discover Berlin', content: 'The place to be.' } }

    context 'when requests attributes are valid' do
      before { post "/magazines/#{magazine_id}/articles", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when request attribues are not valid' do
      before { post "/magazines/#{magazine_id}/articles", params: {} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Title can't be blank, Content can't be blank/)
      end
    end
  end

  describe 'PUT /magazines/:magazine_id/articles/:id' do
    let(:valid_attributes) { { title: 'What to see in Tarragona' } }

    before { put "/magazines/#{magazine_id}/articles/#{id}", params: valid_attributes }

    context 'when article exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the item' do
        updated_article = Article.find(id)
        expect(updated_article.title).to match(/What to see in Tarragona/)
      end
    end

    context 'when the item does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Article/)
      end
    end
  end

  describe 'DELETE /magazines/:magazine_id/articles/:id' do
    before { delete "/magazines/#{magazine_id}/articles/#{id}" }

    it 'returns status 204' do
      expect(response).to have_http_status(204)
    end
  end

end