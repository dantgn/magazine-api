FactoryGirl.define do
  factory :magazine do
    title { Faker::Lorem.word }
  end
end