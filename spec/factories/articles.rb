FactoryGirl.define do
  factory :article do
    title { Faker::Lorem.word }
    content { Faker::Lorem.paragraph }
    magazine_id nil
  end
end