class Article < ApplicationRecord
  belongs_to :magazine

  validates_presence_of :title, :content
end
