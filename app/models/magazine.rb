class Magazine < ApplicationRecord
  has_many :articles

  validates_presence_of :title
end
