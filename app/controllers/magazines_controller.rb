class MagazinesController < ApplicationController

  before_action :set_magazine, only: [:show, :update, :destroy]

  def index
    @magazines = Magazine.all
    json_response @magazines
  end

  def create
    @magazine = Magazine.create!(magazine_params)
    json_response(@magazine, :created)
  end

  def show
    json_response @magazine
  end

  def update
    @magazine = Magazine.update(magazine_params)
    head :no_content
  end

  def destroy
    @magazine.destroy
    head :no_content
  end

  private

  def magazine_params
    params.permit(:title)
  end

  def set_magazine
    @magazine = Magazine.find(params[:id])
  end

end
