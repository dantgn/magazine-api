class ArticlesController < ApplicationController

  before_action :set_magazine
  before_action :set_article, only: [:show, :update, :destroy]

  def index
    json_response @magazine.articles
  end

  def create
    @magazine.articles.create!(article_params)
    # @article = Article.create!(article_params)
    json_response(@magazine.articles.last, :created)
  end

  def show
    json_response(@article)
  end

  def update
    @article.update(article_params)
    head :no_content
  end

  def destroy
    @article.destroy
    head :no_content
  end

  private

  def set_magazine
    @magazine = Magazine.find(params[:magazine_id])
  end

  def set_article
    @article = Article.find(params[:id])
  end

  def article_params
    params.permit(:title, :content)
  end

end
